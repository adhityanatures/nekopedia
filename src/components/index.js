import NavbarComponents from './NavbarComponents';
import Banner from './Banner';
import DataComponents from './DataComponents';
import CatBreeds from './CatBreeds';

export { NavbarComponents, Banner, DataComponents, CatBreeds }
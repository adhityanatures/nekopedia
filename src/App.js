import React, { Component } from 'react'
import { NavbarComponents, Banner, DataComponents, CatBreeds } from "./components";
import { API_URL } from './utils/constant'
import axios from 'axios'
import { Container, Row } from 'react-bootstrap';

export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       menus: [],
       images: []
    }
  }

  componentDidMount() {
    axios
      .get(API_URL+"breeds")
      .then(res => {
        const menus = res.data;
        this.setState({ menus });
      })
      .catch(error => {
        console.log(error);
      })
  }
  
  render() {
    const {menus} = this.state
    return (
      <div className="App">
        <Container>
          <NavbarComponents />
          <Banner />
          <DataComponents />
          <Row>
          {menus && menus.map((menu) => (
              <CatBreeds
              key={menu.id}
              menu={menu}
              />
            ))
            }
          </Row>
        </Container>
      </div>
    )
  }
}



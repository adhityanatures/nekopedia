import React from 'react'
import { Row, Nav, Navbar } from 'react-bootstrap'

const NavbarComponents = () => {
    return (
        <Row>
            <Navbar bg="transparent" expand="lg">
                <Navbar.Brand href="#home"><h3><strong>Neko</strong>pedia</h3></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#link">Encyclopedia</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
        </Navbar>
        </Row>
    )
}

export default NavbarComponents

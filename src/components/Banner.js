import React from 'react'
import { Image, Button, Row, Col } from 'react-bootstrap'

const Banner = () => {
    return (
        
            <Row>
              <Col md={6}>
                <div className="d-flex h-100">
                  <div className="justify-content-center align-self-center">
                    <h2>
                      <strong>How Cat-astrophic are you ?</strong> <br />
                      Live long and pawspurr.
                    </h2>
                    <p>What do you mean those clean black pants weren’t for me?</p>
                    <Button variant="warning" size="lg">Discover more</Button>
                  </div>
                </div>
              </Col>

              <Col md={6}>
                  <div className="justify-content-center align-self-center">
                      <Image src="cats.png" width="100%" />
                  </div>
              </Col>
            </Row>
        
    )
}

export default Banner

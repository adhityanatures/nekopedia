import React from 'react'
import { Col, Card, Button } from 'react-bootstrap'


const CatBreeds = ({ menu }) => {
    
    return (
            <Col md={4} className="mt-4">
                <Card className="shadow" style={{ borderRadius: 15 }}>
                    {/* <Card.Img style={{ borderTopLeftRadius: 15, borderTopRightRadius: 15 }} variant="top" src="https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg" /> */}
                    <Card.Body>
                        <Card.Title><strong>{menu.name}</strong></Card.Title>
                        <Card.Text>
                            <strong>Origin :</strong> {menu.origin}
                        </Card.Text>
                        <Card.Text>
                            <strong>Temperament :</strong> {menu.temperament}
                        </Card.Text>
                        <Card.Text>
                            <strong>Lifespan :</strong> {menu.life_span} Yr
                        </Card.Text>
                        <hr />
                        <Button variant="warning">More details</Button>
                    </Card.Body>
                </Card>
            </Col>
    )
}

export default CatBreeds
